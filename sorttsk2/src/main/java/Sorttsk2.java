/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

/**
 *
 * @author spoty
 */
import java.util.Arrays;
public class Sorttsk2 {
    

    public static void directSelectionSort(String[] array) {
        for (int i = 0; i < array.length; i++) {
            String min = null;
            int index = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo(array[i]) <0) {
                    if (min != null) {
                        if (array[j].compareTo(min) > 0) {
                            continue;
                        }
                    }

                    min = array[j];
                    index = j;
                }
            }

            var temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }

    }

    public static void bubbleSort(String[] array) {
         boolean wasSwapped;

        do {
            wasSwapped = false;

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].compareTo(array[i+1]) > 0) {
                    var temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;

                    wasSwapped = true;
                }
            }
        } while (wasSwapped);

        
    }

    public static void insertionSort(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (array[j+1].compareTo(array[j]) > 0) {
                    break;
                }

                var temp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = temp;
            }
        }

        
    }

    public static void main(String[] args) {
       

        final String[] originalArray = {
                "Ivanov",
                "Petrov",
                "Albert",
                "Petrov2",
                "Ivanov",
                "Petya",
                "Oleg",
        };

        System.out.println("Original:\t\t" + Arrays.toString(originalArray));

        final String[] rightAnswer = {
                "Albert",
                "Ivanov",
                "Ivanov",
                "Oleg",
                "Petrov",
                "Petrov2",
                "Petya",
        };

        System.out.println("Right answer:\t" + Arrays.toString(rightAnswer));

        var array = originalArray.clone();
        directSelectionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));

        array = originalArray.clone();
        insertionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.equals(array, rightAnswer));
    }

}
